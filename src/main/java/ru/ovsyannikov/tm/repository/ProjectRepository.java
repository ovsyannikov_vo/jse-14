package ru.ovsyannikov.tm.repository;

import ru.ovsyannikov.tm.entity.Project;
import ru.ovsyannikov.tm.service.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();
    private HashMap<String,List<Project>> projectsA = new HashMap<>();

    public Project addToMap(final Project project){
        List<Project> projectsInMap = projectsA.get(project.getName());
        if (projectsInMap == null) projectsInMap = new  ArrayList<>();
        projectsInMap.add(project);
        projectsA.put(project.getName(),projectsInMap);
        return project;
    }

    /*public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }*/

    public Project create(final String name, final String description, final Long userId, final String creatorName, final String executorName) {
        final Project project = new Project(name, description, userId, creatorName, executorName);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final Long userId, final String creatorName) {
        final Project project = new Project(name, userId, creatorName);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description, final String executorName) {
        final Project project = findByID(id);
        if (project == null) return null;
        String oldName = project.getName();
        List<Project> projectListOld = findByName(oldName);
        if(projectListOld == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setExecutorName(executorName);
        if(!oldName.equals(name) && projectListOld.size()>1) {
            List<Project> projectListNew = new ArrayList<>();
            projectListNew.add(project);
            projectListOld.remove(project);
            projectsA.remove(oldName);
            projectsA.put(oldName,projectListOld);
            projectsA.put(name,projectListNew);
        }else{
            projectsA.remove(oldName);
            projectsA.put(name,projectListOld);
        }

        return project;
    }

    public Project update(final Long id, final String executorName) {
        final Project project = findByID(id);
        if (project == null) return null;
        project.setId(id);
        project.setExecutorName(executorName);
        //Project.create(project.getName(), project.getDescription(), project.userId,  String creatorName)
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public Project findByIndex(int index) {
        if (index < 0 || index > projects.size() - 1) return null;
        return projects.get(index);
    }

    public List<Project> findByName(final String name) {
        List<Project> result = new ArrayList<>();
        if(projectsA.get(name) == null) return null;
        for (Project project: projectsA.get(name)){
            result.add(project);
        }
        return result;
    }

    public Project findByID(final Long id) {
        if (id == null) return null;
        for (final Project project: projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project removeByID(final Long id) {
        final Project project = findByID(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /*public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }*/

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public List<Project> findAll(){
        return projects;
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : findAll()) {
            final Long IdUser = project.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(project);
        }
        return result;
    }

}
