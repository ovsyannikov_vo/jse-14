package ru.ovsyannikov.tm.service;

import ru.ovsyannikov.tm.entity.User;
import ru.ovsyannikov.tm.enumerated.Role;
import ru.ovsyannikov.tm.repository.UserRepository;
import java.util.List;

public class UserService {
    private final UserRepository userRepository;
    public User currentUser;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String useMD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.getMessage();
        }
        return null;
    }

    public User createUser(String login, String password, Role role, String firstName, String lastName) {
        if (login == null) return null;
        if (password == null) return null;
        if (role == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.createUser(login, useMD5(password), role, firstName, lastName);
    }

    public boolean checkPassword(final User user, final String password) {
        return user.getPassword().equals(useMD5(password));
    }

    public User findByLogin(String login) {
        if (login == null) return null;
        return userRepository.findByLogin(login);
    }

    public User findById(Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User updateByLogin(String login, String password, String firstName, String lastName) {
        if (login == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateByLogin(login, useMD5(password), firstName, lastName);
    }

    public User updateById(Long id, String password, String firstName, String lastName) {
        if (id == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateById(id, useMD5(password), firstName, lastName);
    }

    public User updatePasswordByLogin(String login, String password) {
        if (login == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordByLogin(login, useMD5(password));
    }

    public User updatePasswordById(Long id, String password) {
        if (id == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordById(id, useMD5(password));
    }

    public User removeByLogin(String login) {
        if (login == null) return null;
        return userRepository.removeByLogin(login);
    }
    public User removeById(Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}

