package ru.ovsyannikov.tm.service;

import ru.ovsyannikov.tm.entity.Project;
import ru.ovsyannikov.tm.entity.Task;
import ru.ovsyannikov.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /*public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }*/

    /*public Task create(final String name) {
        if (name == null) return null;
        return taskRepository.create(name);
    }*/

    public Task create(final String name, final String description, final Long userId, final String creatorName, final String executorName) {
        if (name == null) return null;
        if (description == null) return null;
        if (creatorName == null) return null;
        if (executorName == null) return null;
        return taskRepository.create(name, description, userId, creatorName, executorName);
    }


    public Task update(Long id, String name, String description, final String executorName) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (executorName == null || executorName.isEmpty()) return null;
        return taskRepository.update(id, name, description,executorName);
    }

    public Task setExecutor(Long id, String executorName) {
        if (id == null) return null;
        if (executorName == null || executorName.isEmpty()) return null;
        return taskRepository.update(id, executorName);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task findByIndex(int index) {
        return taskRepository.findByIndex(index);
    }

    public List<Task> findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public Task findByID(Long id) {
        if (id == null) return null;
        return taskRepository.findByID(id);
    }

    public Task removeByID(Long id) {
        if (id == null) return null;
        return taskRepository.removeByID(id);
    }

    /*public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }*/

    public Task removeByIndex(int index) {
        return taskRepository.removeByIndex(index);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findByProjectIdAndId(Long projectId, Long id) {
        if (projectId == null || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    public List<Task> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

}
