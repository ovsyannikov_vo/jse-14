package ru.ovsyannikov.tm.service;

import ru.ovsyannikov.tm.entity.Project;
import ru.ovsyannikov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /*public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }*/

    /*public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }*/

    public Project create(final String name, final String description, final Long userId, final String creatorName, final String executorName ) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (creatorName == null || creatorName.isEmpty()) return null;
        if (executorName == null || executorName.isEmpty()) return null;
        return projectRepository.create(name, description, userId, creatorName, executorName);
    }

    public Project update(Long id, String name, String description, String executorName) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (executorName == null || executorName.isEmpty()) return null;
        return projectRepository.update(id, name, description, executorName);
    }

    public Project setExecutor(Long id, String executorName) {
        if (id == null) return null;
        if (executorName == null || executorName.isEmpty()) return null;
        return projectRepository.update(id, executorName);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project findByIndex(int index) {
        return projectRepository.findByIndex(index);
    }

    public List<Project> findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project findByID(Long id) {
        if (id == null) return null;
        return projectRepository.findByID(id);
    }

    /*public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }*/

    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeByID(id);
    }

    public Project removeByIndex(int index) { return projectRepository.removeByIndex(index); }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }



}
