package ru.ovsyannikov.tm.repository;

import ru.ovsyannikov.tm.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();

    private HashMap<String,List<Task>> tasksA = new HashMap<>();

    public Task addToMap(final Task task){
        List<Task> tasksInMap = tasksA.get(task.getName());
        if (tasksInMap == null) tasksInMap = new ArrayList<>();
        tasksInMap.add(task);
        tasksA.put(task.getName(),tasksInMap);
        return task;
    }

    /*public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }*/

    public Task create(final String name, final String description, final Long userId, final String creatorName, final String executorName) {
        final Task task = new Task(name, description, userId, creatorName, executorName);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description, final String executorName) {
        final Task task = findByID(id);
        if (task == null) return null;
        String oldName = task.getName();
        List<Task> taskListOld = findByName(oldName);
        if(taskListOld == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setExecutorName(executorName);
        if(!oldName.equals(name) && taskListOld.size()>1) {
            List<Task> taskListNew = new ArrayList<>();
            taskListNew.add(task);
            taskListOld.remove(task);
            tasksA.remove(oldName);
            tasksA.put(oldName,taskListOld);
            tasksA.put(name,taskListNew);
        }else{
            tasksA.remove(oldName);
            tasksA.put(name,taskListOld);
        }
        return task;
    }

    public Task update(final Long id, final String executorName) {
        final Task task = findByID(id);
        if (task == null) return null;
        task.setId(id);
        task.setExecutorName(executorName);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() - 1) return null;
        return tasks.get(index);
    }

    public List<Task> findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        List<Task> result = new ArrayList<>();
        if(tasksA.get(name) == null) return null;
        for (Task task: tasksA.get(name)){
        result.add(task);
        }
        return result;
    }

    public Task findByID(final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeByID(final Long id) {
        final Task task = findByID(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /*public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }*/

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public List<Task> findAll() {
        return tasks;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long IdUser = task.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(task);
        }
        return result;
    }


}
