package ru.ovsyannikov.tm.entity;

public class Project {

    private Long id = System.nanoTime();
    private String name = "";
    private String description = "";
    private Long userId;
    private String creatorName = "";
    private String executorName = "";

    public Project() {
    }

    public Project(String name, String description, Long userId, String creatorName, String executorName) {
        this.name = name;
        this.description = description;
        this.userId = userId;
        this.creatorName = creatorName;
        this.executorName = executorName;
    }

    public Project(String name, Long userId, String creatorName) {
        this.name = name;
        this.userId = userId;
        this.creatorName = creatorName;
    }

    /*public Project(String name) {
    }*/

    public Long getUserId() { return userId; }
    public void setUserId(Long userId) { this.userId = userId; }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatorName() {
       return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", creatorName='" + creatorName + '\'' +
                ", executorName='" + executorName + '\'' +
                '}';
    }

}
